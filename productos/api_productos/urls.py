from django.urls import path
from .views import CrearActualizarProducto, ModificarTallas


app_name = "api_productos"
urlpatterns = [
        path("productos/", CrearActualizarProducto.as_view(), name="crear_actualizar_producto"),
        path("buscar_talla/", ModificarTallas.as_view(), name='modificar_talla')
        ]
