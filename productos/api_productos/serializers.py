from rest_framework import serializers
from .models import Producto
import base64


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ['nombre_producto', 'sku', 'imagenes', 'tags', 'costo', 'estatus', 'tallas']

    def validate_imagenes(self, value):
        elementos = value.encode('utf-8').split(b',')
        cadena_final = b''
        for elemento in elementos:
            codificado = base64.b64encode(elemento)
            cadena_final += codificado
        return cadena_final.decode('utf-8')

    def validate_tags(self, value):
        if len(value.split(',')) > 25:
            raise serializers.ValidationError("Solo se permiten 25 tags")
        return value

    def validate_tallas(self, value):
        numeros = value.split(',')
        tallas_erroneas = []
        for numero in numeros:
            try:
                float(numero)
            except ValueError:
                tallas_erroneas.append(numero)

        if tallas_erroneas:
            raise serializers.ValidationError("Las tallas deben ser números válidos. Tallas inválidas: {}"
                                              .format(','.join(tallas_erroneas)))
        return value