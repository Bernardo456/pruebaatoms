from django.db import models
from django.contrib import admin


class Producto(models.Model):
    class Estatus(models.TextChoices):
        ACTIVE = 'active'
        ARCHIVED = 'archived'
        DRAFT = 'draft'

    nombre_producto = models.CharField(max_length=200)
    sku = models.CharField(max_length=50, unique=True)
    imagenes = models.TextField()
    tags = models.CharField(max_length=500)
    costo = models.DecimalField(decimal_places=2, max_digits=18)
    estatus = models.CharField(max_length=20, choices=Estatus.choices)
    tallas = models.CharField(max_length=200)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    actualizado_en = models.DateTimeField(auto_now=True)


class LogTransacciones(models.Model):

    class Estatus(models.TextChoices):
        EXITO = 'exito'
        ERROR = 'error'

    class Operacion(models.TextChoices):
        INSERTAR = 'insertar'
        ACTUALIZAR = 'actualizar'
        REORDENAR_TALLAS = 'reordenar_tallas'

    class Meta:
        indexes = [
            models.Index(fields=['-fecha_transaccion', ]),
        ]

    fecha_transaccion = models.DateTimeField(auto_now=True,)
    sku = models.CharField(max_length=50)
    estatus = models.CharField(max_length=20, choices=Estatus.choices)
    operacion = models.CharField(max_length=20, choices=Operacion.choices)


class Errores(models.Model):
    log = models.ForeignKey(LogTransacciones, on_delete=models.CASCADE, related_name='errores')
    campo_error = models.CharField(max_length=200)
    detalle_error = models.CharField(max_length=200)


admin.site.register(Producto)
admin.site.register(LogTransacciones)
admin.site.register(Errores)
