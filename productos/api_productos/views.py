from rest_framework.generics import CreateAPIView, UpdateAPIView, ListAPIView
# Create your views here.
from .serializers import ProductSerializer
from .models import Producto
from rest_framework.exceptions import ValidationError
from .transaction_manager import TransactionManager
from .models import LogTransacciones
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from utilities.Ordenador import ordenar
from time import perf_counter
from utilities.OrderedSearch import busqueda_ordenada
from rest_framework.response import Response
from rest_framework import status


class CrearActualizarProducto(CreateAPIView, UpdateAPIView, ListAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductSerializer
    lookup_field = 'sku'
    transaction_manager = TransactionManager()


    def post(self, request, *args, **kwargs):
        retorno = super(CrearActualizarProducto, self).post(request, *args, **kwargs)
        self.transaction_manager.store_succesfull_transaction(request, LogTransacciones.Operacion.INSERTAR)
        return retorno

    def update(self, request, *args, **kwargs):
        retorno = super(CrearActualizarProducto, self).update(request, *args, **kwargs)
        self.transaction_manager.store_succesfull_transaction(request, LogTransacciones.Operacion.ACTUALIZAR)
        return retorno

    def create(self, request, *args, **kwargs):
        pk_field = 'sku'
        try:
            response = super().create(request, args, kwargs)
        except ValidationError as e:
            codes = e.get_codes()
            # si el item ya existe
            if pk_field in codes and 'unique' in codes[pk_field]:
                codes.pop(pk_field)
                lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
                self.kwargs[lookup_url_kwarg] = request.data[pk_field]
                try:
                    return super().update(request, *args, **kwargs)
                except ValidationError as e:
                    self.transaction_manager.store_error_transaction(request, e.args[0], LogTransacciones.Operacion.ACTUALIZAR)
                    raise e
            self.transaction_manager.store_error_transaction(request, e.args[0], LogTransacciones.Operacion.INSERTAR)
            raise e
        return response


class ModificarTallas(APIView):
    transaction_manager = TransactionManager()

    def post(self, request):
        sku = request.data.get('sku')
        talla_buscada = request.data.get('talla_buscada')

        errores = dict()
        if not sku:
            errores['sku'] = ['El sku debe estar incluido en el cuerpo de la petición']
        if not talla_buscada:
            errores['talla_buscada'] = ['La talla buscada debe estar incluida en el cuerpo de la petición']
        else:
            try:
                talla_buscada = float(talla_buscada)
            except:
                errores['talla_buscada'] = ['La talla buscada debe ser un número válido']
                talla_buscada = 'Talla mala'

        if errores:
            self.transaction_manager.store_error_transaction(request, errores, LogTransacciones.Operacion.ACTUALIZAR)
            return Response(errores, status=status.HTTP_400_BAD_REQUEST)

        modelo = get_object_or_404(Producto, sku=sku)
        tallas = [int(talla) for talla in modelo.tallas.split(',')]
        tallas_ordenadas = ordenar(tallas)
        modelo.tallas = ','.join([str(talla) for talla in tallas_ordenadas])
        modelo.save()
        self.transaction_manager.store_succesfull_transaction(request, LogTransacciones.Operacion.REORDENAR_TALLAS)
        inicio = perf_counter()
        existe_talla = busqueda_ordenada(tallas_ordenadas, talla_buscada)
        fin = perf_counter()
        tiempo_busqueda = fin - inicio
        reporte = {
            'existe_talla': existe_talla,
            'tiempo_busqueda': tiempo_busqueda,
            'tallas_ordenadas': tallas_ordenadas
        }
        return Response(reporte, )
