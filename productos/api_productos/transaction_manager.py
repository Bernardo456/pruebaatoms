from .models import LogTransacciones, Errores


class TransactionManager:

    def store_error_transaction(self, request, error_list, operacion):
        sku = request.data.get('sku', 'SIN SKU')
        log = LogTransacciones(sku=sku, estatus=LogTransacciones.Estatus.ERROR, operacion=operacion)
        log.save()
        errores = []
        for nombre_campo in error_list:
            detalle_errores = error_list[nombre_campo]
            cadena_detalle_errores = ','.join(detalle_errores)
            error = Errores(log=log, campo_error=nombre_campo, detalle_error=cadena_detalle_errores)
            errores.append(error)
        Errores.objects.bulk_create(errores)

    def store_succesfull_transaction(self, request, operacion):
        sku = request.data.get('sku', 'SIN SKU')
        log = LogTransacciones(sku=sku, estatus=LogTransacciones.Estatus.EXITO, operacion=operacion)
        log.save()
