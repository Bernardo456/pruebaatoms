from django.urls import path
from .views import MonitorTransacciones, MonitorErrores


app_name = "monitor_transacciones"
urlpatterns = [
        path("", MonitorTransacciones.as_view(), name="monitor_transacciones"),
        path("errores/", MonitorErrores.as_view(), name="monitor_errores"),
        ]
