from django.shortcuts import render
from django.views.generic import TemplateView, ListView
from api_productos.models import LogTransacciones, Errores


# Create your views here.
class MonitorTransacciones(ListView):
    model = LogTransacciones
    queryset = LogTransacciones.objects.order_by('-fecha_transaccion').all()
    paginate_by = 10


class MonitorErrores(ListView):
    model = Errores
    queryset = Errores.objects.select_related('log').order_by('-log__fecha_transaccion').all()
    paginate_by = 10
