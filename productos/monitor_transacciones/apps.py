from django.apps import AppConfig


class MonitorTransaccionesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'monitor_transacciones'
