
def busqueda_ordenada(lista_ordenada, numero_buscado):
    low = 0
    high = len(lista_ordenada) - 1
    mid = 0
    #compara al número con el valor intermedio como la lista está ordenada
    while low <= high:
        mid = (high + low) // 2
        if lista_ordenada[mid] < numero_buscado:
            low = mid + 1
        elif lista_ordenada[mid] > numero_buscado:
            high = mid - 1
        else:
            return True

    return False
